const state = {
  text: ''
}

const getters = {}

const actions = {
  fetchItem ({ commit }, newText) {
    commit('newTextMutation', newText)
  }
}

const mutations = {
  newTextMutation (state, newText) {
    state.text = newText
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
