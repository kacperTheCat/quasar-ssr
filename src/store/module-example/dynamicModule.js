import axios from 'axios'

export default {
  namespaced: true,
  state: () => ({
    preloadedData: ''
  }),
  actions: {
    preloadDataAction: ({ commit }, dataToPreload) => {
      return new Promise(async resolve => {
        axios.get('https://swapi.co/api/species').then(res => {
          commit('preloadDataMutation', res.data)
          resolve()
        })
      })
    }
  },
  mutations: {
    preloadDataMutation: (state, data) => {
      state.preloadedData = data.results
    }
  }
}
